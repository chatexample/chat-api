
# Socket.IO Chat API

Przykładowa aplikacja chat (API) oparta o websockety.

## Uruchomienie

```
$ npm install
$ npm start
```

Domyślnie aplikacja nasłuchuje na porcie 3000